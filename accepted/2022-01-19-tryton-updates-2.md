# Project Data

| Title | Tryton updates for Debian 2 |
| ----- | ----- |
| **Funding source** | LTS |
| **Submitter** | Mathias Behrle <mathiasb@m9s.biz>  |
| **Executor** |  Mathias Behrle |
| **Reviewer** | Neil Williams |
| **Proposal date** | 2022-01-19 |
| **Approval date** | 2022-02-08 |
| **Request for bids** | Not needed, executor is submitter |
| **Project management issue** | (insert link when project starts) |
| **Completion date** | (insert date when completed) |

----------------------------------------

# Rationale

The goal of this project is to complete the available Tryton modules in 
Debian main and to automate this task as much as possible.

# Description

- Add some official tools to ease/automate the task of packaging Tryton
  modules.
- Package and upload the current published, but not yet packaged Tryton modules.

## Project Details

Please see submitter bid section below;

## Completion Criteria

* Publication of the tools in the official team repos
  at git@salsa.debian.org:tryton-team/tryton-team-tools.git
* Package and upload the missing official 53 modules
  (110 currently packaged, 163 available)
	including their evtl. dependencies.

## Estimated Effort

Depending on the tooling and achievable automation the effort should amount
to 1875 - 3750 EUR (25-50 hours of work).

# Submitter Bid

- Debian tooling for the Tryton suite
  - cookiecutter template or script to automate the creation of packages for
    Tryton modules
  - scripting for automatic creation of autopkgtests

- Completion of the Tryton suite
  - Packaging of missing published modules of the project
  - Packaging of missing dependencies
