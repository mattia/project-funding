Instructions: This template is intended as a guide for providing progress
reports as comments to the dedicated project management issue for an active
project.

Starting with the "# Progress Report" header below, copy the template into the
comment text area.  Replace any placeholders (text enclosed in parentheses)
below as you complete your report.

---

# Progress Report

## Summary

(Provide a short, 2 to 3 sentence, summary of the work which was completed since
the previous report or since the work began.  Include significant
accomplishments and unexpected challenges or setbacks which were encountered.)

## Accomplishments

(Document the tasks and/or items which were accomplished during the time
interval covered by the progress report.  The preferred format is a bulleted
list.)

## Challenges

(Document any challenges, setbacks, unexpected difficulties, etc. which may have
hindered the work during the time interval covered by the progress report.  This
section should include tasks which were accomplished but which required
significantly more time and effort than originally planned.  If any of the items
listed in this section require external assistance or additional effort above
and beyond the original project scope, make note of that.  The preferred format
is a bulleted list.)

## Next Steps

(Describe the concrete next steps for the project.  Be certain to provide enough
information to enable a rough estimation of the effort involved in the next
steps.)
