---
title: "Projects"
date: 2022-01-27T14:43:31-05:00
description: "Projects which have been proposed, accepted, or completed under the LTS funding initiative."
---

## Proposed

| Project name | Status |
|:---          |    ---:|
| 	       |        |

## Accepted

| Project name | Status |
|:---          |    ---:|
|    [Gradle 6.4 Packaging](https://salsa.debian.org/freexian-team/project-funding/-/issues/19) | **`Accepted`** |
|   Tryton update project: [part 1](https://salsa.debian.org/freexian-team/project-funding/-/issues/21) and [part 2](https://salsa.debian.org/freexian-team/project-funding/-/issues/22) | **`Accepted`** |

## Completed

| Project name | Status |
|:---          |    ---:|
| [Better no-dsa support in PTS](https://salsa.debian.org/freexian-team/project-funding/-/blob/master/completed/2020-12-pts-no-dsa.md) | **`Completed`** |

