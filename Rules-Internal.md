# Rules for internal projects

Those rules apply to project proposals submitted by Freexian. Those
projects are usually justified by internal needs or customer requests.

## Who can propose projects?

Only Freexian can propose such projects. The projects are directly added
to the "accepted" bucket. They don't need any external approval.

## Who can make bids?

Anybody can make a bid.

## How will the winning bid be selected?

Freexian's management will select the winning bid.

## Who will review the work?

Freexian will appoint a reviewer, either internal or external.
